### 瑞安编程站
 **更新自2020.4.13** 

 _已在多平台同步更新，链接如下：_ 

 **github（主仓库）：** https://github.com/Ryanyuanao/ryanyuanao.github.io

 **预览地址：** https://ryanyuanao.xyz

 **码云（gitee）：** https://gitee.com/Ryanyuanao/ryanyuanao.github.io

 **预览地址：** https://ryanyuanao.gitee.io/ryanyuanao.github.io

 **coding：** https://ryanyuanao.coding.net/p/ryanyuanao.github.io/git

 **预览地址：** https://n.dowy.cn

 **gitlab：** https://gitlab.com/Ryanyuanao/ryanyuanao.gitlab.io

 **预览地址：** http://z.ryanyuanao.xyz

 **阿里云Code托管平台：** http://code.aliyun.com/ryanyuanao/ryanyuanao.github.io

 **bitbucket：** https://bitbucket.org/ryanyuanao/ryanyuanao.bitbucket.io

 **预览地址：** https://ryanyuanao.bitbucket.io

 _任务清单表：_
 
| 项目 | 进度 |
| ------ | ------ |
| 游戏大全主页完善 | √ |
| Html5特效大全主页完善 | √ |
| 聊天机器人类型增多 | × |
| 倒计时开放平台主页完善 | × |
| 便民工具主页完善 | × |
| 身份验证页面美化 | × |
| JS版植物大战僵尸游戏Bug处理 | × |
| 工具箱功能增多 | × |
| Html5特效增多 | × |
| 游戏增多 | × |
| 网站主页美化 | × |
| 网站启用www前缀 | × |
| 网站全站使用https | × |
| 完善网站笔记平台功能 | × |
| 为网站开发电脑版客户端 | × |
| 为网站开发手机APP | × |
| ······ | - |

**如果您能帮我们把以上的×变为√的话，请联系邮箱：yuanao10@qq.com！** 

